const Boards = require('../models/boards');

/**
 * Details about the stage
 * 1: TODO
 * 2: In Progress
 * 3: Completed
 */

// Handle Board create on POST.
exports.board_create_post = async function(req, res) {
  try {
    let newItem = (req.body.stage != undefined) ? req.body : { stage: 1, ...req.body };
    let newBoard = await Boards.create(newItem);
    res.status(201).send(newBoard.toJSON());
  } catch(error) {
    res.status(422).send(error);
  }
};

// Display detail page for a specific board.
exports.board_detail = async function(req, res) {
  try {
    const board = await Boards.findByPk(req.params.id);
    if (board === null) {
      res.status(204).send({});
    } else {
      res.status(200).send(board.toJSON());
    }
  } catch(error) {
    res.status(422).send(error);
  }
};

// Handle Board update on PUT.
exports.board_update = async function(req, res) {
  try {
    let permitStages = [1, 2, 3];
    if (permitStages.includes(req.body.stage)) {
      await Boards.update({ stage: req.body.stage }, { where: { id: req.params.id } });
      let board = await Boards.findByPk(req.params.id);
      res.status(200).send(board.toJSON());
    } else {
      res.status(400).send("no requirement");
    }
  } catch(error) {
    res.status(422).send(error);
  }
};

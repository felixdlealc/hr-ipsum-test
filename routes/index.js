var express = require('express');
var router = express.Router();

const controller = require('../controllers/boards');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('<p>HTML Data</p>');
});

/* GET home page. */
router.get('/boards', function(req, res, next) {
  res.send('<p>HTML Data Boards</p>');
});

// POST request for one Board.
router.post('/boards', controller.board_create_post);

// GET request for one Board.
router.get('/boards/:id', controller.board_detail);

// PUT request for one Board.
router.put('/boards/:id', controller.board_update);

module.exports = router;
